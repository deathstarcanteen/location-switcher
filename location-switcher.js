/**
 * `location-switcher`
 *
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class LocationSwitcher extends Polymer.Element {
  static get is() { return 'location-switcher'; }
  static get properties() {
    return {
      location: {
        type: String,
        value: 'EyeOnIt'
      },

      locations: {
        type: Array,
        value: function() {
          return [
            {
              name: 'EyeOnIt',
              id: 1,
              secondary: [
                {
                  name: 'Building 1',
                  id: 2,
                  parent: 1,
                  tertiary: [
                    {
                      name: 'Room 1',
                      parent: 2,
                    },
                    {
                      name: 'Room 2',
                      parent: 2,
                    },
                    {
                      name: 'Room 3',
                      parent: 2,
                    },
                    {
                      name: 'Room 4',
                      parent: 2,
                    },
                    {
                      name: 'Room 5',
                      parent: 2,
                    }
                  ],
                },
                {
                  name: 'Building 2',
                  parent: 1,
                  id: 3,
                  tertiary: [
                    {
                      name: 'Room 1',
                      parent: 3,
                    },
                    {
                      name: 'Room 2',
                      parent: 3,
                    },
                    {
                      name: 'Room 3',
                      parent: 3,
                    },
                    {
                      name: 'Room 4',
                      parent: 3,
                    },
                    {
                      name: 'Room 5',
                      parent: 3,
                    }
                  ],
                },
              ]
            },
          ]
        }
      },

      newLocation: {
        type: String,
        value: 'No Location'
      }
    };
  }

  setLevel(event) {
    const index = event.target.dataset.index;
    const level = event.target.dataset.level;
    if(level === 'tertiary') {
      const indexes = index.split('/');

      this.set('secondaryIndex', indexes[0]);
      this.set('tertiaryIndex', indexes[1]);
    } else {
      this.set('secondaryIndex', index);
      this.set('tertiaryIndex', null);
    }
  }

  _computeIndex(locationIndex, secondaryIndex) {
    if(typeof secondaryIndex !== 'undefined') {
      return locationIndex + '/' + secondaryIndex;
    } else {
      return locationIndex;
    }
  }

  _computeSecondary(locations, locationIndex) {
    if(!locations) return;
    if(!locations[locationIndex]) return;
    return locations[locationIndex].secondary;
  }

  _computeTertiary(locations, locationIndex, secondaryIndex) {
    if(!locations) return;
    if(!locations[locationIndex]) return;
    return locations[locationIndex].secondary[secondaryIndex].tertiary;
  }

  changeLocation() {
    this.shadowRoot.querySelector('.container').removeAttribute('hidden');
  }

  _computeParent(locations, level, locationIndex, secondaryIndex) {
    if(level === 'secondary') {

    }
    if(level === 'tertiary') {
      return this.locations.name + '/' + [locationIndex].secondary
    }
  }

  add() {
    this.shadowRoot.querySelector('.list').setAttribute('hidden', '');
    this.shadowRoot.querySelector('.add-new').removeAttribute('hidden');
    this.shadowRoot.querySelector('#go').setAttribute('hidden', '');
  }

  next() {
    this.shadowRoot.querySelector('.belongs').removeAttribute('hidden');
    this.shadowRoot.querySelector('.list').removeAttribute('hidden');
    this.shadowRoot.querySelector('.add-new').setAttribute('hidden', '');
    this.shadowRoot.querySelector('#back').removeAttribute('hidden');
    this.shadowRoot.querySelector('#save').removeAttribute('hidden');
  }

  go(event) {
    this.location = this.shadowRoot.querySelector('.iron-selected').innerHTML;
    this.shadowRoot.querySelector('.container').setAttribute('hidden', '');
  }

  save() {
    this.shadowRoot.querySelector('.belongs').setAttribute('hidden', '');
    this.shadowRoot.querySelector('#go').removeAttribute('hidden');
    this.shadowRoot.querySelector('#back').setAttribute('hidden', '');
    this.shadowRoot.querySelector('#save').setAttribute('hidden', '');

    if(!this.secondaryIndex && !this.tertiaryIndex) {
      this.push('locations', {name: this.newLocation, parent: 0});
    }

    if(this.tertiaryIndex) {
      this.push('locations.' + this.secondaryIndex + '.secondary.' + this.tertiaryIndex + '.tertiary', {name: this.newLocation, parent: 0});
    } else {
      let newObject = {name: this.newLocation, parent: 1};
      this.push('locations.' + this.secondaryIndex + '.secondary', newObject);
    }

    let newLocations = this.get('locations');

    this.set('locations', []);
    this.set('locations', newLocations);
  }

  back() {
    this.shadowRoot.querySelector('.belongs').setAttribute('hidden', '');
    this.shadowRoot.querySelector('#go').removeAttribute('hidden');
    this.shadowRoot.querySelector('#back').setAttribute('hidden', '');
    this.shadowRoot.querySelector('#save').setAttribute('hidden', '');
  }
}

window.customElements.define(LocationSwitcher.is, LocationSwitcher);
